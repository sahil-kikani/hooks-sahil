import { React } from 'react'
import './App.css'
import Layout from './components/Layout'
import Reducer from './components/Reducer'
import Themecontext from './components/Theme-context'
import useTheme from './components/useTheme'

function App () {
  const [theme, toggletheme] = useTheme()

  return (
    <div className="App">
      <Themecontext.Provider value={theme}>
      <button
      onClick={toggletheme}
      >
       Chanage Image
      </button>
      <Layout/>
      </Themecontext.Provider>
      <Reducer/>
    </div>
  )
}

export default App
