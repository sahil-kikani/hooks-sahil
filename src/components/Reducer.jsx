import React, { useReducer } from 'react'
import Img3 from '../img/img3.jpg'
import Img4 from '../img/img4.jpg'
import Img5 from '../img/img5.jpg'

export default function Reducer () {
  const [state, dispatch] = useReducer((state, action) => {
    switch (action.type) {
      case 'gm':
        return { time: 'Hello Goodmorning', img: Img3 }
      case 'ga':
        return { time: 'Hello Goodafternoon', img: Img4 }
      case 'gn':
        return { time: 'Hello Goodnight', img: Img5 }
      default:
        throw new Error()
    }
  }, { time: '' })
  return (
    <>
      <button onClick={() => dispatch({ type: 'gm' })}>Good morning</button>
      <button onClick={() => dispatch({ type: 'ga' })}>Good afternoon</button>
      <button onClick={() => dispatch({ type: 'gn' })}>Good night</button>
      <p>{state.time}</p><br/>
     <img style={{ height: '50vh' }} src={state.img}/><br/>
    </>
  )
}
