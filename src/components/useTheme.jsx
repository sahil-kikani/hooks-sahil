import { useState } from 'react'

import { themes } from './Theme-context'

const useTheme = () => {
  const [theme, setTheme] = useState(themes.dark)

  const toggletheme = () => {
    theme === themes.dark ? setTheme(themes.light) : setTheme(themes.dark)
  }

  return [theme, toggletheme]
}

export default useTheme
