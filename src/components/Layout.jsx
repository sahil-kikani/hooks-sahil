import { React, useContext } from 'react'
import ThemeContext from './Theme-context'

function Layout () {
  const theme = useContext(ThemeContext)
  console.log(theme)
  return (
        <div style={{ width: 'auto', margin: '10px auto', color: theme.color }} >
            <img src={theme.backgroundImage}/>
        </div>
  )
}
export default Layout
