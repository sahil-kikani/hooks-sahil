import React from 'react'
import Img1 from '../img/img1.jpg'
import Img2 from '../img/img2.jpg'

export const themes = {
  dark: {
    color: 'white',
    backgroundImage: Img2
  },
  light: {
    color: 'black',
    backgroundImage: Img1
  }
}

const ThemeContext = React.createContext(themes)

export default ThemeContext
